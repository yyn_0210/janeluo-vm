/*
 * 简洛工作室版权所有，保留所有权利。
 */

package net.janeluo.framework.base;

/**
 * 概述: 查询通用接口
 * 功能描述: 查询通用接口
 * 创建记录: 2011/02/10
 * 修改记录: 
 */
public interface BaseSearchCond {

	/**
	 * 排序字段取得
	 * 
	 * @return 排序字段
	 */
	public String getSort();


	/**
	 * 排序方式取得
	 * 
	 * @return 排序方式
	 */
	public String getDir();
}
