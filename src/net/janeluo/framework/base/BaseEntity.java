/*
 * 简洛工作室版权所有，保留所有权利。
 */

package net.janeluo.framework.base;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

@SuppressWarnings("serial")
public abstract class BaseEntity implements BaseModel {
	protected String fingerPrint;

	public BaseEntity() {
	}

	public String getFingerPrint() {
		return fingerPrint;
	}

	public void setFingerPrint(String fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	public abstract Serializable getPK();

	public String toString() {
		
		return ToStringBuilder.reflectionToString(this);
		
	}

}