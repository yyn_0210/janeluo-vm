/*
 * 简洛工作室版权所有，保留所有权利。
 */

package net.janeluo.framework.base;

/**
 * 概述: 查询通用接口实现
 * 功能描述: 查询通用接口实现
 * 创建记录: 2011/02/10
 * 修改记录: 
 */
public class BaseSearchCondImpl implements BaseSearchCond {

	/** 排序字段 */
	protected String sort;
	
	/** 排序方式 */
	protected String dir;
	
	/**
	 * 排序字段取得
	 * 
	 * @return 排序字段
	 */
	public String getSort() {
		return sort;
	}

	/**
	 * 排序字段设定
	 * 
	 * @param sort排序字段
	 */
	public void setSort(String sort) {
		this.sort = sort;
	}

	/**
	 * 排序方式取得
	 * 
	 * @return 排序方式
	 */
	public String getDir() {
		return dir;
	}

	/**
	 * 排序方式设定
	 * 
	 * @param dir排序方式
	 */
	public void setDir(String dir) {
		this.dir = dir;
	}

}
