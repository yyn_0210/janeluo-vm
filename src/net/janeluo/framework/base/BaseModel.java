/*
 * 简洛工作室版权所有，保留所有权利。
 */

package net.janeluo.framework.base;

import java.io.Serializable;

public interface BaseModel extends Serializable {

	public abstract String getFingerPrint();
}