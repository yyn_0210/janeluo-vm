/*
 * 简洛工作室版权所有，保留所有权利。
 */
package net.janeluo.framework.utility;

import org.apache.commons.lang.time.FastDateFormat;


/**
 * 概述: 系统常量定义
 * 功能描述: 系统常量定义
 * 修改记录: 
 */
public final class CommonDefineUtils {

	/** 日期格式定义 */
	public final static FastDateFormat TIMESTAMP_FORMAT = FastDateFormat.getInstance("yyyy/MM/dd HH:mm:ss.SSS");
	
	/** 日期格式定义 */
	public final static FastDateFormat DATETIME_FORMAT = FastDateFormat.getInstance("yyyy/MM/dd HH:mm");

	/** 日期格式定义 */
	public final static FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy/MM/dd");

	/** 日期格式定义 */
	public final static FastDateFormat YEAR_FORMAT = FastDateFormat.getInstance("yyyy");

	/**  */
	public final static String APPLICATIONNAME = "svc";
	
	private CommonDefineUtils(){}

}
