/*
 * 简洛工作室版权所有，保留所有权利。
 */
package net.janeluo.framework.dao.hibernate;

import org.hibernate.criterion.Example.PropertySelector;
import org.hibernate.type.Type;

class PropertySelectorOr implements PropertySelector {
	private static final long serialVersionUID = 5304161953706828385L;

	PropertySelectorOr() {
	}

	public boolean include(Object object, String propertyName, Type type) {
		return object != null;
	}
}
