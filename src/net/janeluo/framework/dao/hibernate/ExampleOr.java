/*
 * 简洛工作室版权所有，保留所有权利。
 */
package net.janeluo.framework.dao.hibernate;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.*;

class ExampleOr extends Example {
	private static final long serialVersionUID = 5541778933883923703L;

	protected class SimpleOrExpression extends SimpleExpression {
		private static final long serialVersionUID = -8112081311033921422L;

		protected SimpleOrExpression(String s, Object obj, String s1) {
			super(s, obj, s1);
		}
	}

	private ExampleOr(Object entity) {
		super(entity, new PropertySelectorOr());
	}

	public static ExampleOr a(Object entity) {
		if (entity == null)
			throw new NullPointerException("null example");
		else
			return new ExampleOr(entity);
	}

	protected void appendPropertyCondition(String s, Object obj,
			Criteria criteria, CriteriaQuery criteriaquery,
			StringBuffer stringbuffer) throws HibernateException {
		if (obj != null) {
			String sql = toSqlString(criteria, criteriaquery);
			if (stringbuffer.length() > 1 && sql.length() > 0)
				stringbuffer.append(" or ");
			stringbuffer.append(sql);
		}
	}
}
