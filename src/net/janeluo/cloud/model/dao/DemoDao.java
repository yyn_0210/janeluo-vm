package net.janeluo.cloud.model.dao;

import java.util.List;

import net.janeluo.cloud.model.entity.Demo;
import net.janeluo.framework.dao.hibernate.BaseHibernateDAOImpl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository("demoDao")
public class DemoDao extends BaseHibernateDAOImpl<Demo>{

    public DemoDao() {
        super(Demo.class);
    }
    
    public List<Demo> findAll() {
        DetachedCriteria beautyCriteria = getDetachedCriteria(); 
//        
        
//        Criteria criteria = currentSession().createCriteria(getClass());
        
//        return findByCriteria(criteria);
//        List<?> aa = findByCriteria(beautyCriteria);
        List<?> aa = super.findAll();
        System.out.println(aa);
        return null;
        
//        HibernateCallback<List<Demo>> action = new HibernateCallback<List<Demo>>() {
//
//            public List<Demo> doInHibernate(Session session) throws HibernateException {
//
//                Query query = session.createSQLQuery("select * from t_demo");
//                
//                return query.list();
//
//            }
//        };
//
//        return (List<Demo>) execute(action);
    }
}
