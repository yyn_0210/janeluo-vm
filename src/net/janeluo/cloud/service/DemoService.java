package net.janeluo.cloud.service;

import java.util.List;

import javax.annotation.Resource;

import net.janeluo.cloud.model.dao.DemoDao;
import net.janeluo.cloud.model.entity.Demo;

import org.springframework.stereotype.Service;

@Service("demoService")
public class DemoService {
    
    @Resource
    private DemoDao demoDao;
    
    public void hello() {
        System.out.println("揍你");
        List<Demo> lst = demoDao.findAll();
        System.out.println(lst);
    }

}
